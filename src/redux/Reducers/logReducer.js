import { ADD_TO_LOG, CLEAR_LOG } from '../type/index'

export const initialState = {
    logs: [],
  };  

  export const logReducer = (state = initialState, action) => {
    switch (action.type) {
      case ADD_TO_LOG:
        return {
          ...state,
          logs: [...state.logs, action.payload],
        };
      case CLEAR_LOG:
        return {
          ...state,
          logs: [],
        };
      default:
        return state;
    }
  };