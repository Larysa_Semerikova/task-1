import { ADD_TO_LOG, CLEAR_LOG } from "../type/index";

export const addToLog = (entry) => ({
  type: ADD_TO_LOG,
  payload: entry,
});

export const clearLog = () => ({
  type: CLEAR_LOG,
});
