import { combineReducers } from "redux";
import { logReducer } from "./logReducer";

const rootReducer = combineReducers({
  logs: logReducer,
});

export default rootReducer;
