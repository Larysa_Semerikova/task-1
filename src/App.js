import React, { useState } from "react";
import Button from "./components/Button/index";
import styles from "./App.module.scss";
import { useDispatch, useSelector } from "react-redux";
import { addToLog, clearLog } from "./redux/actions/index"

function App() {
  const dispatch = useDispatch()
  const logs = useSelector((state) => state.logs)
  console.log(logs)

  function startTimer(id, duration) {
    let date = new Date();
    let log = date.toLocaleTimeString();
    let click = new Date(date.getTime() + duration).toLocaleTimeString();

    let str = `№:${id}: ${log} - ${click} (${duration / 1000} sec)`

    const timerId = setTimeout(() => {
      console.log(str)
      dispatch(addToLog(str))
    }, duration);
  }

  function clearAllTimers() {
    dispatch(clearLog())
  }

  return (
    <div className={styles.container}>
      <Button text="1 sec" onClick={() => startTimer(1, 1000)} />
      <Button text="2 sec" onClick={() => startTimer(2, 2000)} />
      <Button text="3 sec" onClick={() => startTimer(3, 3000)} />
      <Button text="Clear" onClick={clearAllTimers} />
    </div>
  );
}

export default App;
